<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>API DSE-B</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/grayscale.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TobatKuy</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">API Jadwal Solat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index2.php">API Cuaca</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index3.php">API Jam</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="about.php">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <h1 class="mx-auto my-0 text-uppercase">Jam</h1>
        <h2 class="text-white-50 mx-auto mt-2 mb-5">Jam adalah sebuah unit waktu. Lama sebuah jam adalah 1/24 (satu perduapuluh empat) hari. Satu jam bisa dibagi menjadi unit waktu yang lebih kecil lagi.Satu jam terdiri dari: 60 menit , 3600 detik . Pukul juga menunjukkan satuan waktu. Jam bermakna "masa atau jangka waktu", sedangkan pukul bermakna "saat atau waktu".</h2>
          <h2 class="text-white-50 mx-auto mt-2 mb-5">Dalam bahasa Indonesia, jika ingin mengungkapkan "saat atau waktu", digunakan kata 'pukul' Jika ingin mengungkapkan "masa atau jangka waktu", digunakan kata 'jam' (contoh: Di sekolah selama delapan jam). Jam juga dapat berarti "benda penunjuk waktu"</h2>
      <a href="#about" class="btn btn-primary js-scroll-trigger">KUY !!</a>
      </div>
    </div>
  </header>

  <!-- About Section -->
  <section id="about" class="about-section text-center">
  <form method="GET" action="#">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-white mb-4">Pilih Jam Dari Daerah Yang Disediakan</h2>
          <p class="text-white-50">Dengan keberagaman waktu di indonesia maka terjadi perbedaan waktu untuk setiap daerahnya dimana disini disediakan beberapa waktu untuk mmasing-masing daerah:</p>
          
            <h3 class="text-white mb-4" selected>Pilih Daerah</h3>
            <select name="asalkota" >
            <option value="Aceh">Aceh</option>
            <option value="Ambon">Ambon</option>
            <option value="Bandung">Bandung</option>
            <option value="Bali">Bali</option>
            <option value="Banjarmasin">Banjarmasin</option>
            <option value="Banten">Banten</option>
            <option value="Gorontalo">Gorontalo</option>
            <option value="Jambi">Jambi</option>
            <option value="Jayapura">Jayapura</option>
            <option value="Jakarta">Jakarta</option>
            <option value="Kendari">Kendari</option>
            <option value="Kupang">Kupang</option>
            <option value="Lampung">Lampung</option>
            <option value="Manado">Manado</option>
            <option value="Mamuju">Mamuju</option>
            <option value="Medan">Medan</option>
            <option value="Manokwari">Manokwari</option>
            <option value="Makassar">Makassar</option>
            <option value="Mataram">Mataram</option>
            <option value="Padang">Padang</option>
            <option value="Palangkaraya">Palangkaraya</option>
            <option value="Palu">Palu</option>
            <option value="Palembang">Palembang</option>
            <option value="Pangkalpinang">Pangkalpinang</option>
            <option value="Pekanbaru">Pekanbaru</option>
            <option value="Pontianak">Pontianak</option>
            <option value="Samarinda">Samarinda</option>
            <option value="Semarang" >Semarang</option>
            <option value="Sofifi">Sofifi</option>
            <option value="Surabaya">Surabaya</option>
            <option value="Tanjungpinang">Tanjungpinang</option>
            <option value="Yogyakarta">Yogyakarta</option>
            <option value="Amerika">Amerika</option>
          </select>
          <input type="submit" value="Cek Jam" >
          
        </div>
      </div><br>
        
    </div>
    </form>
  </section>

  <?php
            if(isset($_GET['asalkota'])){
                $kota = $_GET['asalkota'];

                $url3 = file_get_contents('http://api.farzain.com/jam.php?id='.$kota.'&apikey=5pFOuIFrlz9ozSva5f22rqV25');
                      $hasil2 = json_decode($url3);
                       if($hasil2->status=="success"){
                          echo "<h1><center> Jam Sekarang ";
                          echo "Waktu :".$hasil2->data->timezone."<br>";
                          echo "Waktu :".$hasil2->time->date."<br>";
                          echo "Waktu :".$hasil2->time->time."<br>";
                          echo "Waktu :".$hasil2->location->address."<br>";
                       }
            } 
                        
        ?>

  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
      Copyright &copy; TobatKuy 2019
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/grayscale.min.js"></script>

</body>

</html>
