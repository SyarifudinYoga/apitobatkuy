<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>API DSE-B</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/grayscale.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TobatKuy</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">API Jadwal Solat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index2.php">API Cuaca</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index3.php">API Jam</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="about.php">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <h1 class="mx-auto my-0 text-uppercase">Cuaca</h1>
        <h2 class="text-white-50 mx-auto mt-2 mb-5">Cuaca adalah seluruh fenomena yang terjadi di atmosfer Bumi atau sebuah planet lainnya. Cuaca biasanya merupakan sebuah aktivitas fenomena dalam waktu beberapa hari.
        <h2 class="text-white-50 mx-auto mt-2 mb-5">Cuaca rata-rata dengan jangka waktu yang lebih lama dikenal sebagai iklim. Aspek cuaca ini diteliti lebih lanjut oleh ahli klimatologi, untuk tanda-tanda perubahan iklim.</h2>
         <a href="#about" class="btn btn-primary js-scroll-trigger">KUY !!</a>
      </div>
    </div>
  </header>

  <!-- About Section -->
  <section id="about" class="about-section text-center">
  <form method="GET" action="#">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-white mb-4">Pilih  Jadwal Cuaca</h2>
          <p class="text-white-50">Dengan keberagaman waktu cuaca di indonesia maka terjadi perbedaan waktu cuaca untuk setiap daerahnya dimana disini disediakan beberapa waktu untuk mmasing-masing daerah:</p>
          
            <h3 class="text-white mb-4" selected>Pilih Daerah</h3>
            <select name="asalkota" >
            <option value="Aceh">Aceh</option>
            <option value="Ambon">Ambon</option>
            <option value="Bandung">Bandung</option>
            <option value="Bali">Bali</option>
            <option value="Banjarmasin">Banjarmasin</option>
            <option value="Banten">Banten</option>
            <option value="Gorontalo">Gorontalo</option>
            <option value="Jambi">Jambi</option>
            <option value="Jayapura">Jayapura</option>
            <option value="Jakarta">Jakarta</option>
            <option value="Kendari">Kendari</option>
            <option value="Kupang">Kupang</option>
            <option value="Lampung">Lampung</option>
            <option value="Manado">Manado</option>
            <option value="Mamuju">Mamuju</option>
            <option value="Medan">Medan</option>
            <option value="Manokwari">Manokwari</option>
            <option value="Makassar">Makassar</option>
            <option value="Mataram">Mataram</option>
            <option value="Padang">Padang</option>
            <option value="Palangkaraya">Palangkaraya</option>
            <option value="Palu">Palu</option>
            <option value="Palembang">Palembang</option>
            <option value="Pangkalpinang">Pangkalpinang</option>
            <option value="Pekanbaru">Pekanbaru</option>
            <option value="Pontianak">Pontianak</option>
            <option value="Samarinda">Samarinda</option>
            <option value="Semarang" >Semarang</option>
            <option value="Sofifi">Sofifi</option>
            <option value="Surabaya">Surabaya</option>
            <option value="Tanjungpinang">Tanjungpinang</option>
            <option value="Yogyakarta">Yogyakarta</option>
          </select>
          <input type="submit" value="Cek Cuaca" >
          
        </div>
      </div><br>
        
    </div>
    </form>
  </section>

  <?php
            if(isset($_GET['asalkota'])){
                $kota = $_GET['asalkota'];
            
                $url2 = file_get_contents('http://api.farzain.com/cuaca.php?id='.$kota.'&apikey=5pFOuIFrlz9ozSva5f22rqV25');
                    $hasil = json_decode($url2);
                        if($hasil->status == 200){
                            echo "<center>tempat : ".$hasil->respon->tempat."<br>";
                            echo "cuaca : ".$hasil->respon->cuaca."<br>";
                            echo "deskripsi : ".$hasil->respon->deskripsi."<br>";
                            echo "suhu : ".$hasil->respon->suhu."<br>";
                            echo "kelembapan : ".$hasil->respon->kelembapan."<br>";
                            echo "udara : ".$hasil->respon->udara."<br>";
                            echo "angin : ".$hasil->respon->angin."<br>";
                        }
            } 
                        
        ?>


  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
      Copyright &copy; TobatKuy 2019
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/grayscale.min.js"></script>

</body>

</html>
