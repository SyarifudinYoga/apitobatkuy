<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>API DSE-B</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/grayscale.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">TobatKuy</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">API Jadwal Solat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index2.php">API Cuaca</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index3.php">API Jam</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="about.php">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  
       

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <h1 class="mx-auto my-0 text-uppercase">About Us</h1>
        <h2 class="text-white-50 mx-auto mt-2 mb-5">Sistem Informasi TobatKuy dimana bertujuan untuk mengingatkan umat muslim untuk waktu solat dan melaksanakan solat 5 waktu , 
        dengan banyaknya daerah di Indonesia dan ragam waktu yang ada maka tiap daerah memiliki waktu yang berbeda-beda dalam melaksanakan solat , selain itu juga pada sistem ini terdapat
        informasi untuk perkiraan cuaca yang bisa menjadi informasi untuk perkiraan cuaca di hari ini suaya juga memudahkan bagi umat muslim yang ingin berangkat ke mesjid 
        dan bisa mengantisipasi dalam berbagai cuaca.</h2>
         <a href="#about" class="btn btn-primary js-scroll-trigger" data-target="#member" data-toggle="modal">Member</a>
      </div>
    </div>
  </header>



  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
      Copyright &copy; TobatKuy 2019
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/grayscale.min.js"></script>

  <div id="member" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                
               <h4 class="modal-tittle" align="center">Member</h4>

              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <form method="post" action="batalOrder.php" enctype="multipart/form-data">
                <center>
                <img src="iqbal.jpg" style="width:150px;"><br>
                <label><b>Iqbal Abdul Jabbar</b></label><br>
                <label><b>3411161080</b></label><br>
                <img src="yo.jpg" style="width:150px;"><br>
                <label><b>Syarifudin Yoga Pinasty</b></label><br>
                <label><b>3411161125</b></label><br>
                </center>
                <div class="modal-footer">
                <label><b>Official TobatKuy</b></label><br>
                </div>
              </form>
            </div>
          </div>       
        </div>
      </div>


</body>

</html>
